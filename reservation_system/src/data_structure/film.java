package data_structure;


public class film {
	public String id;
	public String name;
	public String url;
	public int classification;
	public String description;
	public int length;
	public double score1;
	public int score2;
	public String hall;
	public String[] timelist;
	public hall[] halls;
	public int number;
	
	public film(){
	}
	
	public film(String id,String name,String url,int classification,String description,int length,double score1,int score2,String hall,String playtime,int number){
		this.id = id;
		this.name = name;
		this.url = url;
		this.classification = classification;
		this.description = description;
		this.length = length;
		this.score1 = score1;
		this.score2 = score2;
		this.hall = hall;
		this.number = number;
		if(hall.equals("Îäµ±")||hall.equals("ÉÙÁÖ")||hall.equals("»ªÉ½")){
			this.halls = new bighall[number];
			for(int i=0;i<number;i++){
				halls[i] = new bighall();
			}
		}
		else{
			this.halls = new smallhall[number];
			for(int i=0;i<number;i++){
				halls[i] = new smallhall();
			}
		}
	}
	
	public film(String id,String name,String url,String classification,String description,int length,double score1,int score2,String playtime,String hall){
		this.id = id;
		this.name = name;
		this.url = url;
		this.description = description;
		this.length = length;
		this.score1 = score1;
		this.score2 = score2;
		this.hall = hall;
		this.timelist = playtime.split("¡¢");
		this.number = this.timelist.length;
		if(classification.equals("ÆÕÍ¨"))
			this.classification = 0;
		else if(classification.equals("±£×o"))
			this.classification = 6;
		else if(classification.equals("ÝoŒ§"))
			this.classification = 15;
		else if(classification.equals("ÏÞÖÆ"))
			this.classification = 18;
		if(hall.equals("Îä®”")||hall.equals("ÉÙÁÖ")||hall.equals("ÈAÉ½")){
			this.halls = new bighall[number];
			for(int i=0;i<number;i++){
				halls[i] = new bighall();
			}
		}
		else{
			this.halls = new smallhall[number];
			for(int i=0;i<number;i++){
				halls[i] = new smallhall();
			}
		}
	}
}
