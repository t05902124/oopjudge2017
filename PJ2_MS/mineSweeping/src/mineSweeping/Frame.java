package mineSweeping;

import timer.*;
import database.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;

//扫雷主界面
public class Frame extends JFrame {
	private JButton start, exit, cheat, seekhero;
	private JLabel number;
	private JTextField leftNumber; // 显示剩余雷数
	private JTextField rightNumber;// 显示时间
	private JToolBar jt; // 工具栏(存放以上所有组件)
	private Panel panel;
	private Timer t;
	private String userName;

	public Frame() {
		start = new JButton("开始");
		exit = new JButton("结束");
		number = new JLabel("剩余雷数:");
		cheat = new JButton("作者是我爸爸");
		seekhero = new JButton("英雄榜");
		leftNumber = new JTextField(" 99 ");
		leftNumber.setEditable(false);
		rightNumber = new JTextField(" 0 ");
		rightNumber.setEditable(false);
		jt = new JToolBar();
		jt.add(start);
		jt.add(exit);
		jt.add(number);
		jt.add(leftNumber);
		jt.add(rightNumber);
		jt.add(seekhero);
		jt.add(cheat);
		add(jt, BorderLayout.NORTH);

		panel = new Panel(this);
		add(panel);
		// t = new Timer(this);

		userName = JOptionPane.showInputDialog(null, null, "请输入用户名:", JOptionPane.INFORMATION_MESSAGE);
		if (userName == null || userName.length() == 0) // 若用户没有输入用户名则使用默认用户名NTUSweeper
			userName = "NTUSweeper";
		panel.setUserName(userName);

		// 记录游戏开始时间
		Calendar ca = Calendar.getInstance(); // 获取当前系统时间
		panel.setHour(ca.get(Calendar.HOUR));
		panel.setMinute(ca.get(Calendar.MINUTE));
		panel.setSecond(ca.get(Calendar.SECOND));

		// t.printTime();

		// 点击开始按钮触发事件(界面变回初始状态)
		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				leftNumber.setText(" 99 ");
				// rightNumber.setText(" 0 ");
				panel.setNumber(99);
				panel.setShownNumber(99);
				Calendar ca = Calendar.getInstance(); // 获取当前系统时间
				panel.setHour(ca.get(Calendar.HOUR));
				panel.setMinute(ca.get(Calendar.MINUTE));
				panel.setSecond(ca.get(Calendar.SECOND));
				JButton[][] buttons = panel.getButtons();
				int[][] mine = panel.getMine();
				for (int i = 0; i < 16; i++)
					for (int j = 0; j < 30; j++) {
						buttons[i][j].setEnabled(true);
						buttons[i][j].setText("");
						mine[i][j] = 0;
					}
				Random ran = new Random();
				int position = 0;
				for (int i = 0; i < 99; i++) {
					position = ran.nextInt(16 * 30); // 随机产生雷的位置
					if (mine[position / 30][position % 30] == -1) // 产生的99个雷的位置不允许重复
						i--; // 再i++后相当于i没变化
					else
						mine[position / 30][position % 30] = -1;
				}

				// 填入每格的数据
				for (int i = 0; i < 16; i++)
					for (int k = 0; k < 30; k++) {
						if (mine[i][k] != -1) // 不是雷
						{// 扫描周围8个格子，确定雷数
							for (int j = k - 1; j <= k + 1; j++) {
								if (j >= 0 && j < 30) // 在雷区内
								{
									if (mine[i][j] == -1)
										mine[i][k]++;

									if ((i - 1) >= 0) // 在雷区内
										if (mine[i - 1][j] == -1)
											mine[i][k]++;

									if ((i + 1) < 16) // 在雷区内
										if (mine[i + 1][j] == -1)
											mine[i][k]++;
								}
							}
						}
					}
				// 显示地雷位置（测试时使用）
				/*
				 * for(int i=0;i<5;i++) { for(int k=0;k<5;k++) {
				 * System.out.printf("%3d",mine[i][k]); }
				 * System.out.printf("\n"); }
				 */
				// 把修改后的数据送回
				panel.setButtons(buttons);
				panel.setMine(mine);
			}
		});

		cheat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.cheating();
			}
		});

		seekhero.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<Record> tmp = new ArrayList<Record>();
				tmp = database.DataBaseManage.Search();
				String s = new String();
				if (tmp.size() == 0)
					JOptionPane.showMessageDialog(null, "暂无纪录");
				else {
					if (tmp.size() == 1)
						s = "" + tmp.get(0).getName() + "\t" + tmp.get(0).getTime() + "\n";
					else if (tmp.size() == 2) {
						if (tmp.get(0).getTime() < tmp.get(0).getTime())
							s = "" + tmp.get(0).getName() + "\t" + tmp.get(0).getTime() + "\n" + tmp.get(1).getName()
									+ "\t" + tmp.get(1).getTime();
						else
							s = "" + tmp.get(1).getName() + "\t" + tmp.get(1).getTime() + "\n" + tmp.get(0).getName()
									+ "\t" + tmp.get(0).getTime();
					} 
					else {
						MyComparator mc = new MyComparator();
						Collections.sort(tmp, mc);
						s = "" + tmp.get(0).getName() + "\t" + tmp.get(0).getTime() + "\n" + tmp.get(1).getName() + "\t"
								+ tmp.get(1).getTime() + "\n" + tmp.get(2).getName() + "\t" + tmp.get(2).getTime();
					}

					JOptionPane.showMessageDialog(null, "英雄榜\n玩家\t时间\n" + s);
				}
			}
		});

		// 点击结束按钮结束游戏
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}

	public JTextField getLeftNumber() {
		return leftNumber;
	}
	//
	// public JTextField getRightNumber() {
	// return rightNumber;
	// }

	public void setLeftNumber(JTextField leftNumber) {
		this.leftNumber = leftNumber;
	}
}

// public void setRightNumber(JTextField rightnumber) {
// this.rightNumber = rightnumber;
// }
// }