
public class UnknownCmdException extends Exception {
private String errMessage;

public UnknownCmdException(String errMessage){
	this.errMessage = errMessage;
}

public String getMessage(){
	return errMessage;
}
}
