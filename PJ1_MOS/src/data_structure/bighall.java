package data_structure;

import java.util.*;

public class bighall implements hall{
	private int[][] seats;//-1:inavailable,even:available,odd:reserved;
	/*
	 * 0:normal available 1:normal reserved
	 * 2:blue available 3:blue reserved
	 * 4:yellow available 5:yellow reserved
	 * 6:red available 7:red reserved
	 * then, seats[i][j]/2 = 0,1,2,3:normal,blue,yellow,red
	 * seats%2 = 0: available, seats%2 = 1: reserved
	*/
	public bighall(){
		seats = new int[13][40];
		int i,j;
		
		for(j=0;j<40; j++){//A,0
			if(j>=8&&j<=31){
				seats[0][j]=0;
			}
			else{
				seats[0][j]=-1;
			}
		}
		
		for(j=0;j<40;j++){//B,1
			if(j>=5&&j<=34){
				seats[1][j] = 0;
			}
			else{
				seats[1][j] = -1;
			}
		}
		
		for(i=2;i<=5;i++){//C,D,E,F,2,3,4,5
			for(j=0;j<40;j++){
				if(j>=1&&j<=38){
					seats[i][j] = 0;
				}
				else{
					seats[i][j] = -1;
				}
			}
		}
		
		for(j=0;j<40;j++){//G,6
			if(j>=8&&j<=31)
				seats[6][j] = 2;
			else if(j>=1&&j<=38)
				seats[6][j] = 0;
			else seats[6][j] = -1;
		}
		
		for(j=0;j<40;j++){//H,7
			if(j>=10&&j<=29)
				seats[7][j] = 4;
			else if(j>=8&&j<=31)
				seats[7][j] = 2;
			else if(j>=1&&j<=38)
				seats[7][j] = 0;
			else seats[7][j] = -1;
		}
		
		for(j=0;j<40;j++){//I,8;J,9
			if(j>=14&&j<=25){
				seats[8][j] = 6;
				seats[9][j] = 6;
			}
			else if(j>=10&&j<=29){
				seats[8][j] = 4;
				seats[9][j] = 4;
			}
			else if(j>=8&&j<=31){
				seats[8][j] = 2;
				seats[9][j] = 2;
			}
			else if(j>=1&&j<=38){
				seats[8][j] = 0;
				seats[9][j] = 0;
			}
			else{
				seats[8][j] = -1;
				seats[9][j] = -1;
			}
		}
		
		for(j=0;j<40;j++){//K,10
			seats[10][j] = seats[7][j];
		}
		for(j=0;j<40;j++){//L,11
			if(j>=10&&j<=31)
				seats[10][j] = 4;
			else if(j>=6&&j<=35)
				seats[10][j] = 2;
			else if(j>=1&&j<=39)
				seats[10][j] = 0;
			else seats[10][j] = -1;
		}
		for(j=0;j<40;j++){//M,12
			if(j>=1&&j<=8){
				seats[12][j] = 0;
			}
			else if(j>=31&&j<=38)
				seats[12][j] = 0;
			else seats[12][j] = -1;
		}
		for(i=0;i<=10;i++){
			seats[i][12] = -1;
			seats[i][13] = -1;
			seats[i][26] = -1;
			seats[i][27] = -1;
		}
	}
	public boolean reserve(char row,int b){
		int a = row - 'A';
		if(a<0||a>12){
			//throw new reserveException(reserveException.Type.SEATILLEGAL);
			return false;
		}
		else if(b>=40){
			//throw new reserveException(reserveException.Type.SEATILLEGAL);
			return false;
		}
		else if(seats[a][b]==-1){
			//throw new reserveException(reserveException.Type.SEATILLEGAL);
			return false;
		}
		else if(seats[a][b]%2==1){
			return false;
		}
		else{
			seats[a][b] = seats[a][b]  + 1;
			return true;
		}
	}
	public boolean checkseats(char row, int b){
		int a = row - 'A';
		if(row<'A'||row>'M'){
			return false;
		}
		else if(b<0||b>39){
			return false;
		}
		else if(seats[a][b]<0){
			return false;
		}
		else if(seats[a][b]%2==1){
			return false;
		}
		else return true;
	}
	public int getblue(){
		int i,j;
		int n = 0;
		for(i=0;i<13;i++){
			for(j=0;j<40;j++){
				if(seats[i][j]==2)
					n++;
			}
		}
		return n;
	}
	public int getgray(){
		int i,j;
		int n = 0;
		for(i=0;i<13;i++){
			for(j=0;j<40;j++){
				if(seats[i][j]==0)
					n++;
			}
		}
		return n;
	}
	public int getyellow(){
		int i,j;
		int n = 0;
		for(i=0;i<13;i++){
			for(j=0;j<40;j++){
				if(seats[i][j]==4)
					n++;
			}
		}
		return n;
	}
	public int getred(){
		int i,j;
		int n = 0;
		for(i=0;i<13;i++){
			for(j=0;j<40;j++){
				if(seats[i][j]==6)
					n++;
			}
		}
		return n;
	}
	public ArrayList<Ticket> reserven(int n){
		int j;
		char i;
		Ticket temp;
		ArrayList<Ticket> t = new ArrayList<Ticket>();
 		int count = 0;
		if(n<=0){
			System.out.println("小于等于零咋订票？");
			return null;
		}
		for(i='A';i<='M';i++){
			for(j = 0;j<40;j++){
				if(checkseats(i,j))
					count++;
			}
		}
		if(count<n){
			return null;
		}
		count = 0;
		for(i='A';i<='M';i++){
			for(j = 0;j<40;j++){
				if(count>=n){
					return t;
				}
				if(checkseats(i,j)){
					count++;
					reserve(i,j);
					temp = new Ticket(i,j);
					t.add(temp);
				}
					
			}
		}
		return t;
	}

	public boolean returnseat(char row,int b){
		int a = row - 'A';
		seats[a][b] = seats[a][b]-1;
		return true;
	}
	public ArrayList<Ticket> reserveSqt(int n){
		int j;
		char i;
		Ticket temp;
		ArrayList<Ticket> t = new ArrayList<Ticket>();
 		int count = 0;
		if(n<=0){
			System.out.println("小于等于零咋订票？");
			return null;
		}
		for(i='A';i<='M';i++){
			count = 0;
			for(j = 0;j<40;j++){
				if(checkseats(i,j))
					count++;
				else count = 0;
				if(count>=n){
					for(;count>0;count--){
						reserve(i,j);
						temp = new Ticket(i,j);
						t.add(temp);
						j--;
					}
					return t;
				}
			}
		}
		return null;
	}
	
	public ArrayList<Ticket> reserveGR(char row,int n){
		int j;
		Ticket temp;
		ArrayList<Ticket> t = new ArrayList<Ticket>();
 		int count = 0;
		if(n<=0){
			System.out.println("小于等于零咋订票？");
			return null;
		}
			for(j = 0;j<40;j++){
				if(checkseats(row,j))
					count++;
			}
		if(count<n){
			return null;
		}
		count = 0;
			for(j = 0;j<40;j++){
				if(count>=n){
					return t;
				}
				if(checkseats(row,j)){
					count++;
					reserve(row,j);
					temp = new Ticket(row,j);
					t.add(temp);
				}
					
			}		
		return t;
	}
	
	public ArrayList<Ticket> reserveGR(String color,int n){
		int j;
		char i;
		int nColor;
		Ticket temp;
		ArrayList<Ticket> t = new ArrayList<Ticket>();
 		int count = 0;
		if(n<=0){
			System.out.println("小于等于零咋订票？");
			return null;
		}
		if(color.equals("red")){
			count = getred();
			nColor = 6;
		}
		else if(color.equals("blue")){
			count = getblue();
			nColor = 2;
		}
		else if(color.equals("yellow")){
			count = getyellow();
			nColor = 4;
		}
		else if(color.equals("gray")){
			count = getgray();
			nColor = 0;
		}
		else{
			System.out.println("输入行数非法");
			return null;
		}
		if(count<n){
			return null;
		}
		count = 0;
		for(i='A';i<='M';i++){
			for(j = 0;j<40;j++){
				if(count>=n){
					return t;
				}
				if(checkseats(i,j)&&seats[i-'A'][j]==nColor){
					count++;
					reserve(i,j);
					temp = new Ticket(i,j);
					t.add(temp);
				}
					
			}
		}
		return t;
	}
	public int availableSeatsNumberAtGivenRow(char row){
		int j;
		int count = 0;
		for(j=0;j<40;j++){
			if(checkseats(row,j)){
				count++;
			}
		}
		return count;
	}
}
