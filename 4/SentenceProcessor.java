
public class SentenceProcessor {
public String removeDuplicatedWords(String input){
	int start = 0;								//the location to start search for next white space
	int space = input.indexOf(" ",start);		//the location of a white space
	String word = input.substring(start,space);//find the word
	String result = new String(word);			//create the result string
	start = space + 1;
	//put the first word out of the while loop because it doesn't have a white space before it.
	while(input.indexOf(" ",start)!=-1){		//keep find words until there exits no white spaces
		space = input.indexOf(" ",start);
		word = input.substring(start,space);
		if(result.indexOf(word)==-1){			//judge whether the word is duplicated
			result +=" ";
			result += word;
		}
		start = space + 1;
	}
	word = input.substring(start,input.length());//deal with the last word
	if(result.indexOf(word)==-1){
		result +=" ";
		result += word;
	}
	return result;
} 
public String replaceWord(String deleteWord,String addWord,String input){
	int start = 0;									//the location to start search for next white space
	int space = input.indexOf(" ",start);			//the location of a white space
	String word = input.substring(start, space);	//find the word
	String result;									//create the result string
	if(word.equals(deleteWord))						//judge whether the first word should be repalced
		result = new String(addWord);
	else
		result = new String(word);
	start = space + 1;
	//put the first word out of the while loop because it doesn't have a white space before it.
	while(input.indexOf(" ",start)!=-1){			//keep find words until there exits no white spaces
		space = input.indexOf(" ",start);
		word = input.substring(start,space);
		if(word.equals(deleteWord)){
			result += " ";
			result +=addWord;
		}
		else{
			result +=" ";
			result +=word;
		}
		start = space+1;
	}
	word = input.substring(start,input.length());//judge whether the last word should be repalced
	if(word.equals(deleteWord)){
		result += " ";
		result +=addWord;
	}
	else{
		result +=" ";
		result +=word;
	}
	return result;
}
}