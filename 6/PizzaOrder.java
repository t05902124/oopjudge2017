public class PizzaOrder {
	private Pizza pizza1=null, pizza2=null, pizza3=null;
	private int numberPizzas;
	public boolean setNumberPizzas(int numberPizzas){
		if(numberPizzas<1||numberPizzas>3){
			return false;
		}
		else{
			this.numberPizzas = numberPizzas;
			return true;
		}
	}
	public void setPizza1(Pizza pizza1){
		this.pizza1 = pizza1;
	}
	public void setPizza2(Pizza pizza2){
		this.pizza2 = pizza2;
	}
	public void setPizza3(Pizza pizza3){
		this.pizza3 = pizza3;
	}
	public double calcTotal(){
		double total = 0;
		if(pizza1!=null){			//before we use an object, we should check whether it exists
			total += pizza1.calcCost();
		}
		if(pizza2!=null){
			total +=pizza2.calcCost();
		}
		if(pizza3!=null){
			total +=pizza3.calcCost();
		}
		return total;
	}
}