package data_structure;

public class reserveException extends Exception {
	public enum Type{
		SALEOUT,AGE,SEQUENTIALOUT,REGIONOUT,SEATILLEGAL
	};
	private Type t;
	
	public reserveException(reserveException.Type t){
		this.t = t;
	}
	public String getType(){
		return t.toString();
	}
}
