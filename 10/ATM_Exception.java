
public class ATM_Exception extends Exception {
	public enum ExceptionTYPE{
		BALANCE_NOT_ENOUGH,AMOUNT_INVALID;
	}
	private ExceptionTYPE  excptionCondition;
	
	public ATM_Exception(ATM_Exception.ExceptionTYPE t){
		this. excptionCondition = t;
	}
	
	public String getMessage(){
		if(this.excptionCondition.equals(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID)){
			return "AMOUNT_INVALID";
		}
		else return "BALANCE_NOT_ENOUGH";
	}
	
}
