package database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import data_structure.*;

public class MovieDataBaseManage {
	public static ArrayList<film> MovieSearchByID(String ID){
		Connection c = null;
	    Statement stmt = null;
	    ArrayList<film> r = new ArrayList<film>();
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM FILM WHERE ID = '" + ID +"';" );
	      while ( rs.next() ) {
	         film tmp = new film(rs.getString("ID"),rs.getString("NAME"),rs.getString("URL"),rs.getString("CLASSIFICATION"),rs.getString("DESCRIPTION"),rs.getInt("LENGTH"),(double)rs.getFloat("SCOREONE"),rs.getInt("SCORETWO"),rs.getString("PLAYTIME"),rs.getString("HALL"));
	         r.add(tmp);
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");
	    return r;
		}
	public static ArrayList<film> MovieSearchByRate(double rate){
		Connection c = null;
	    Statement stmt = null;
	    ArrayList<film> r = new ArrayList<film>();
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM FILM WHERE SCOREONE >= " + rate +";" );
	      while ( rs.next() ) {
	         film tmp = new film(rs.getString("ID"),rs.getString("NAME"),rs.getString("URL"),rs.getString("CLASSIFICATION"),rs.getString("DESCRIPTION"),rs.getInt("LENGTH"),(double)rs.getFloat("SCOREONE"),rs.getInt("SCORETWO"),rs.getString("PLAYTIME"),rs.getString("HALL"));
	         r.add(tmp);
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");
	    return r;
		}
		public static ArrayList<film> GiveMeAllMovie(){
		Connection c = null;
	    Statement stmt = null;
	    ArrayList<film> r = new ArrayList<film>();
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM FILM;" );
	      while ( rs.next() ) {
	         film tmp = new film(rs.getString("ID"),rs.getString("NAME"),rs.getString("URL"),rs.getString("CLASSIFICATION"),rs.getString("DESCRIPTION"),rs.getInt("LENGTH"),(double)rs.getFloat("SCOREONE"),rs.getInt("SCORETWO"),rs.getString("PLAYTIME"),rs.getString("HALL"));
	         r.add(tmp);
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Operation done successfully");
	    return r;
		}
	}
