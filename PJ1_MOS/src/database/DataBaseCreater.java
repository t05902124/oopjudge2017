package database;

import java.sql.*;

public class DataBaseCreater {
	public static void main(String [] argv){
		Create();
	}
	public static void Create(){
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
	      System.out.println("Opened database successfully");

	      stmt = c.createStatement();
	      String sql = "CREATE TABLE TICKET " +
                  "(ID 			   INT     NOT NULL," +
                  " FILMNAME       TEXT    NOT NULL, " +
                  " FILMID		   TEXT	   NOT NULL, " +
                  " PLAYTIME       TEXT    NOT NULL, " + 
                  " ROW        	   TEXT    NOT NULL, " + 
                  " SEATNUMBER     INT	   NOT NULL) "; 
	      stmt.executeUpdate(sql);
	      sql = "CREATE TABLE FILM " +
                  "(ID 			   TEXT    NOT NULL," +
                  " NAME           TEXT    NOT NULL, " + 
                  " URL            TEXT    NOT NULL, " + 
                  " CLASSIFICATION TEXT    NOT NULL, " + 
                  " DESCRIPTION    TEXT    NOT NULL, " +
                  " LENGTH		   INT	   NOT NULL, " +
                  " SCOREONE	   REAL	   NOT NULL, " +
                  " SCORETWO	   INT	   NOT NULL, " +
                  " PLAYTIME 	   TEXT	   NOT NULL, " +
                  " HALL 		   TEXT    NOT NULL) ";
	      stmt.executeUpdate(sql);
	      sql = "CREATE TABLE FLOWNUMBER " +
                  " (ID			   INT	   NOT NULL, " +
                  "  FN 		   INT     NOT NULL) ";
	      stmt.executeUpdate(sql);
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Table created successfully");
	    c = null;
	    stmt = null;
	    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");

		      stmt = c.createStatement();
		      
		      String sql = "INSERT INTO FILM (ID,NAME,URL,CLASSIFICATION,DESCRIPTION,LENGTH,SCOREONE,SCORETWO,PLAYTIME,HALL) " +
	                  "VALUES (" + "'3R47wXhjjLqnLWef6HU155ek'" + " , " + "'異形:聖約 Alien: Covenant'" + " , " + "'http://www.atmovies.com.tw/movie/faen42316204%/'" + " , " + "'輔導'" + " , " + "'好萊塢傳奇大導雷利史考特重返經典科幻鉅作，與《異形》故事連結，並作為以《普羅米修斯》為首的前傳三部曲之第二章節。殖民太空船—聖約號發現了一個未知的世界，由仿生人—大衛所主宰。'" + " , " + "122" + " , " + "7.7" + " , " + "53" + " , " + "'09:40、12:00、14:20、16:40、19:00、21:20、23:40'" + " , " + "'武當'" +");"; 
		      
		      stmt.executeUpdate(sql);

		      sql = "INSERT INTO FILM (ID,NAME,URL,CLASSIFICATION,DESCRIPTION,LENGTH,SCOREONE,SCORETWO,PLAYTIME,HALL) " +
	                  "VALUES (" + "'H55WYTppJHIwZyCfUHJLIbWC'" + " , " + "'亞瑟:王者之劍 King Arthur: Legend of the Sword'" + " , " + "'http://www.atmovies.com.tw/movie/fken33496992/'" + " , " + "'保護'" + " , " + "'蓋瑞奇執導，《環太平洋》查理漢納、裘德洛、艾瑞克巴納等。電影重新詮釋「亞瑟王」與「石中劍」這個流傳千年的傳奇故事，用嶄新的觀點敘述這段膾炙人口的史詩。看亞瑟如何從一無所有的街頭少年，成為偉大的王者。'" + " , " + "127" + " , " + "8.5" + " , " + "47" + " , " + "'09:05、11:30、14:45、17:10、19:35、22:05'" + " , " + "'少林'" +");"; 
		      stmt.executeUpdate(sql);

		      sql = "INSERT INTO FILM (ID,NAME,URL,CLASSIFICATION,DESCRIPTION,LENGTH,SCOREONE,SCORETWO,PLAYTIME,HALL) " +
	                  "VALUES (" + "'Q9xwOoSHCD0Xwfm2EiDGRn3Z'" + " , " + "'逃出絕命鎮 Get Out'" + " , " + "'http://www.atmovies.com.tw/movie/fgen55052448/'" + " , " + "'限制'" + " , " + "'阿奇與阿皮》喬登皮爾首部執導，斯史坦費爾德、凱薩琳凱娜主演。一名年輕非裔男生到他的白人女朋友的莊園家中作客，沒想到遇上讓他永生難忘的種族夢靨。'" + " , " + "104" + " , " + "7.8" + " , " + "76" + " , " + "'11:20、15:20、19:10、23:00'" + " , " + "'華山'" +");"; 
		      stmt.executeUpdate(sql);

		      sql = "INSERT INTO FILM (ID,NAME,URL,CLASSIFICATION,DESCRIPTION,LENGTH,SCOREONE,SCORETWO,PLAYTIME,HALL) " +
	                  "VALUES (" + "'WGd6f01Om27eSmo9X3b6cuXu'" + " , " + "'電影版影子籃球員LAST GAME'" + " , " + "'http://www.atmovies.com.tw/movie/flja93513883/'" + " , " + "'普遍'" + " , " + "'原作漫畫由藤卷忠俊所著，充滿感動與熱血的籃球作品《影子籃球員》，2017年將推出全新的電影。劇情延續動畫本篇，火神與黑子二人升上了二年級後的夏末，將與「奇蹟世代」的成員組隊，迎擊美國街球隊伍。'" + " , " + "91" + " , " + "4.3" + " , " + "78" + " , " + "'09:10、11:15、13:00'" + " , " + "'峨嵋'" +");"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "INSERT INTO FILM (ID,NAME,URL,CLASSIFICATION,DESCRIPTION,LENGTH,SCOREONE,SCORETWO,PLAYTIME,HALL) " +
	                  "VALUES (" + "'pYXEwQEWFBBFarOD5LBJmnTU'" + " , " + "'攻殼機動隊1995 GHOST IN THE SHELL'" + " , " + "'http://www.atmovies.com.tw/movie/fgjp50113568/'" + " , " + "'保護'" + " , " + "'動畫鬼才押井守操刀，攻殼機動隊系列的第一部動畫電影回歸大銀幕。劇情大致來自士郎正宗之原著漫畫《攻殼機動隊》，「公安九課」草薙素子率領其他隊員追查未來世界裡神祕的駭客「傀儡師」。'" + " , " + "83" + " , " + "7.0" + " , " + "23" + " , " + "'17:50、21:30'" + " , " + "'峨嵋'" +");"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "INSERT INTO FILM (ID,NAME,URL,CLASSIFICATION,DESCRIPTION,LENGTH,SCOREONE,SCORETWO,PLAYTIME,HALL) " +
	                  "VALUES (" + "'x7GCv22RgYGfd5l8YdbVqYhZ'" + " , " + "'我和我的冠軍女兒 Dangal'" + " , " + "'http://www.atmovies.com.tw/movie/fdin65074352/'" + " , " + "'普遍'" + " , " + "'「印度3K天王」阿米爾罕再度自製自演的最新電影，改編自印度摔跤選手瑪哈維亞的傳奇感人故事、不可思議的真人真事，父女情深聯手挑戰印度社會性別疆界。摔跤不是男生的事，我們家的女孩不會輸！？'" + " , " + "161" + " , " + "9.4" + " , " + "307" + " , " + "'10:50、13:50、16:50、19:50、21:50'" + " , " + "'崆峒'" +");"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "INSERT INTO FLOWNUMBER (ID,FN) " +
	                  "VALUES (" + "1 , 1"+");"; 
		      stmt.executeUpdate(sql);
		      
		      stmt.close();
		      c.commit();
		      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    System.out.println("Records created successfully");
	  }
}
