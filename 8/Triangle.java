import java.math.*;
public class Triangle extends Shape{
	public Triangle(double length){
		super(length);
	}
	public void setLength(double length){
		this.length = length;
	}
	public double getArea(){
		double area = this.length*this.length*Math.sqrt(3)/4;
		return (double)Math.round(area*100)/100;
	}
	public double getPerimeter(){
		double perimeter = 3*this.length;
		return (double) Math.round(perimeter*100)/100;
	}
	

}