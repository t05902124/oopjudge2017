package mineSweeping;

import java.util.*;
import database.Record;

class MyComparator implements Comparator {

	public int compare(Object o1, Object o2) {
		Record e1 = (Record) o1;
		Record e2 = (Record) o2;
		if (e1.getTime() < e2.getTime())
			return 1;
		else
			return 0;
	}
}