
import java.util.Arrays;

public class SimpleArrayList {
	private Integer[] a;
	
	public SimpleArrayList(){
		a = new Integer[0];
	}
	
	public SimpleArrayList(int n){
		int i;
		a = new Integer[n];
		for(i=0;i<n;i++){
			a[i] = 0;
		}
	}
	
	public void add(Integer i ){
		int n = a.length;
		a = Arrays.copyOf(a, n+1);
		a[n] = i;
	}
	
	public Integer get(int index){
		if(index<0||index>=a.length){
			return null;
		}
		else
			return a[index];
	}
	
	public Integer set(int index, Integer element){
		if(index<0||index>=a.length){
			return null;
		}
		else{
			Integer temp = a[index];
			a[index] = element;
			return temp;
		}
	}
	
	public boolean remove(int index){
		if(index<0||index>=a.length){
			return false;
		}
		else if(a[index]==null){
			return false;
		}
		else{
			int n=a.length;
			int i;
			for(i=index;i<n-1;i++){
				a[i] = a[i+1];
			}
			a = Arrays.copyOf(a, n-1);
			return true;
		}
	}
	
	public void clear(){
		a = new Integer[0];
	}
	
	public int size(){
		return a.length;
	}
	
	public boolean retainAll(SimpleArrayList l){
		boolean bigflag = false;
		boolean littleflag = false;
		int nl = l.a.length;
		for(int i=0;i<this.a.length;i++){
			littleflag = false;
			for(int j=0;j<nl;j++){
				if(this.a[i].equals(l.a[j])){
					littleflag = true;
				}
			}
			if(littleflag==false){
				bigflag = true;
				this.remove(i);
				i--;
			}
		}
		return bigflag;
	}
}