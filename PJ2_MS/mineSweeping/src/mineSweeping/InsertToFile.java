package mineSweeping;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

//用于将数据写入文件
public class InsertToFile {
	private RandomAccessFile randomFile;

	// 打开文件
	public void openFile() {
		try {
			// 打开一个随机访问文件流，按读写方式
			randomFile = new RandomAccessFile("time.txt", "rw");
		} catch (SecurityException securityExpection) {
			System.err.println("You do not have write access to this file");
			System.exit(1);
		} catch (FileNotFoundException fileNotFoundException) {
			System.err.println("Error opening or creating file");
			System.exit(1);
		}
	}

	// 将用户输入的数据写入文件
	public void addRecords(String userName, int time) {
		String timeRecord = time + "";
		String content = userName + " " + time + System.getProperty("line.separator"); // System.getProperty("line.separator")是获得系统的换行符
		try {
			long fileLength = randomFile.length(); // 获得文件长度
			boolean flag = false; // true代表文件中已有该玩家的记录（该玩家玩过该游戏）
			String line = randomFile.readLine(); // 读取一行的数据
			while (line != null) {
				String[] datas = line.split(" "); // 把各项数据拆分开
				if (datas[0].equals(userName)) { // 更新最快时间
					int n = 1; // n代表记录的时间的位数
					int oldTime = Integer.parseInt(datas[1]);
					while (true) // 获取记录的时间的位数
					{
						oldTime /= 10;
						if (oldTime == 0)
							break;
						n++;
					}

					long position = randomFile.getFilePointer();
					randomFile.seek(position - n - 2);
					// 替换最快时间
					randomFile.write((timeRecord + System.getProperty("line.separator")).getBytes());
					flag = true;

					break;
				}
				line = randomFile.readLine(); // 读取新一行的数据
			}

			if (!flag) // 该玩家是第一次玩该游戏
			{
				// 将写文件指针移到文件尾
				randomFile.seek(fileLength);
				randomFile.write(content.getBytes());
			}
		} catch (Exception e) {
			System.err.println("Error operation");
			System.exit(1);
		}
	}

	// 关闭文件
	public void closeFile() {
		if (randomFile != null)
			try {
				randomFile.close();
			} catch (IOException ioException) {
				System.err.println("Error closing file");
				System.exit(1);
			}
	}
}