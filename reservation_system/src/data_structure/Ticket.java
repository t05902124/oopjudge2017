package data_structure;


public class Ticket {
	public int ID;
	public String filmID;
	public String filmname;
	public String playtime;
	public char row;
	public int seatnumber;
	public String hall;
	public Ticket(){
		
	}
	public Ticket(int ID, String filmname,String playtime,char row,int seatnumber){
		this.ID = ID;
		this.filmname = filmname;
		this.playtime = playtime;
		this.row = row;
		this.seatnumber = seatnumber;
	}
	public Ticket(char row,int seatnumber){
		this.row = row;
		this.seatnumber = seatnumber;
	}
	public void set(int ID,String filmID, String filmname,String playtime,String hall){
		this.ID = ID;
		this.filmID = filmID;
		this.filmname = filmname;
		this.playtime = playtime;
		this.hall = hall;
	}
	public Ticket(int ID, String filmID, String filmname, String playtime, char row, int seatnumber){
		this.ID = ID;
		this.filmID = filmID;
		this.filmname = filmname;
		this.playtime = playtime;
		this.row = row;
		this.seatnumber = seatnumber;
	}
}
