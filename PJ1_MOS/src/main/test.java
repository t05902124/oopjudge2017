package main;

import java.time.*;
import java.util.*;
import data_structure.*;
import java.sql.*;
import database.*;

public class test {
	static private ArrayList<Ticket> tickets;// = new ArrayList<Ticket>();
	static private ArrayList<film> films;
	static private User u;
	static private int nuser = 0;
	static private int flownumber;
	
	

	static private film searchFilmID(String ID){
		int len = films.size();
		int i;
		for(i=0;i<len;i++){
			if(films.get(i).id.equals(ID)){
				return films.get(i);
			}
		}
		return null;
	}
	
	static private film searchFilmName(String name){
		int len = films.size();
		int i;
		for(i=0;i<len;i++){
			if(films.get(i).name.equals(name)){
				return films.get(i);
			}
		}
		return null;
	}
	
	static private void reserveTicket(){
		Scanner in = new Scanner(System.in);
		String sTemp;
		ArrayList<Ticket> ticketstemp;
		Ticket tickettemp;
		String playtime;
		int nTemp;
		film f;
		u = new User();
		System.out.println("你好，你叫啥？");
		sTemp = in.nextLine();
		u.setName(sTemp);
		System.out.println("你的年龄？");
		nTemp = in.nextInt();
		u.setAge(nTemp);
		nuser++;
		u.setID(nuser);
		System.out.println("您要定哪一个电影？（ID）");
		sTemp = in.next();
		f = searchFilmID(sTemp);
		if(f==null){
			f = searchFilmName(sTemp);
		}
		if(f==null){
			System.out.println("哥们，没这个电影");
			//in.close();
			return;
		}
		if(u.getAge()<f.classification){
			System.out.println("年龄不够，无法订");
			//in.close();
			return;
		}
		System.out.println("您想要订几点场？（eg: 10:00）");
		playtime = in.next();
		int i;
		for(i=0;i<f.timelist.length;i++){
			if(playtime.equals(f.timelist[i]))
				break;
		}
		if(i == f.halls.length){
			System.out.println("哥们，没这场。");
			//in.close();
			return;
		}
		System.out.println("订几张？");
		nTemp = in.nextInt();
		ticketstemp = f.halls[i].reserven(nTemp);
		if(ticketstemp==null){
			System.out.println("票数不够，订票失败");
			//in.close();
			return;
		}
		int k;
		System.out.print("出票成功，影票id：");;
		for(k=0;k<ticketstemp.size();k++){
			tickettemp = ticketstemp.get(k);
			tickettemp.set(flownumber,f.id, f.name, playtime,f.hall);
			System.out.print(flownumber + ", ");
			flownumber++;
			tickets.add(tickettemp);
		}
		System.out.println("谢谢您");
		//in.close();
		return;
	}
	
	static private void returnTicket(){
		int k;
		String curtime;
		String []temp;
		Integer curhour,curminute;
		Integer tickethour,ticketminute;
		Scanner in = new Scanner(System.in);
		System.out.println("嗨，你要退哪张票？（只输入id，输入一个。）");
		k = in.nextInt();
		int len = tickets.size();
		int i;
		for(i=0;i<len;i++){
			if(tickets.get(i).ID==k){
				break;
			}
		}
		if(i==len){
			System.out.println("没有这个票，你骗我");
			//in.close();
			return;
		}
		System.out.println("现在几点了？(eg: 10:00)");
		curtime = in.next();
		curtime.trim();
		temp = curtime.split(":");
		if(temp.length!=2){
			System.out.println("时间输入错误，退票失败");
			//in.close();
			return;
		}
		try{
			curhour = Integer.valueOf(temp[0]);
			curminute = Integer.valueOf(temp[1]);
		}catch(Exception e){
			System.out.println("时间输入错误，退票失败");
			//in.close();
			return;
		}
		temp = tickets.get(i).playtime.split(":");
		if(temp.length!=2){
			System.out.println("对不起，系统错误，退票失败");
		//	in.close();
			return;
		}
		try{
			tickethour = Integer.valueOf(temp[0]);
			ticketminute = Integer.valueOf(temp[1]);
		}catch(Exception e){
			System.out.println("对不起，系统错误，退票失败");
		//	in.close();
			return;
		}
		if(curhour>tickethour){
			System.out.println("需要在播放前20min前退票，退票失败");
		//	in.close();
			return;
		}
		else if(curhour==tickethour&&(curminute>=ticketminute-20)){
			System.out.println("需要在播放前20min前退票，退票失败");
		//	in.close();
			return;
		}
		else if(curhour==tickethour-1&&(curminute>=ticketminute+60-20)){
			System.out.println("需要在播放前20min前退票，退票失败");
		//	in.close();
			return;
		}else{
			film f = searchFilmID(tickets.get(i).filmID);
			int j;
			for(j=0;j<f.timelist.length;j++){
				if(f.timelist[j].equals(tickets.get(i).playtime))
					break;
			}
			if(j==f.timelist.length){
				System.out.println("对不起，系统错误，退票失败");
		//		in.close();
				return;
			}
			f.halls[j].returnseat(tickets.get(i).row, tickets.get(i).seatnumber);
			tickets.remove(i);
			System.out.println("退票成功，再见");
		//	in.close();
			return;
		}
	}
	
	static private void reserveSequentially(){
		Scanner in = new Scanner(System.in);
		String sTemp;
		ArrayList<Ticket> ticketstemp;
		Ticket tickettemp;
		String playtime;
		int nTemp;
		film f;
		u = new User();
		System.out.println("你好，你叫啥？");
		sTemp = in.nextLine();
		u.setName(sTemp);
		System.out.println("你的年龄？");
		nTemp = in.nextInt();
		u.setAge(nTemp);
		nuser++;
		u.setID(nuser);
		System.out.println("您要定哪一个电影？（ID）");
		sTemp = in.next();
		f = searchFilmID(sTemp);
		if(f==null){
			f = searchFilmName(sTemp);
		}
		if(f==null){
			System.out.println("哥们，没这个电影");
			//in.close();
			return;
		}
		if(u.getAge()<f.classification){
			System.out.println("年龄不够，无法订");
			//in.close();
			return;
		}
		System.out.println("您想要订几点场？（eg: 10:00）");
		playtime = in.next();
		int i;
		for(i=0;i<f.timelist.length;i++){
			if(playtime.equals(f.timelist[i]))
				break;
		}
		if(i == f.halls.length){
			System.out.println("哥们，没这场。");
			//in.close();
			return;
		}
		System.out.println("订几张？");
		nTemp = in.nextInt();
		ticketstemp = f.halls[i].reserveSqt(nTemp);
		if(ticketstemp==null){
			System.out.println("票数不够，订票失败");
			//in.close();
			return;
		}
		int k;
		System.out.print("出票成功，影票id：");;
		for(k=0;k<ticketstemp.size();k++){
			tickettemp = ticketstemp.get(k);
			tickettemp.set(flownumber,f.id, f.name, playtime,f.hall);
			System.out.print(flownumber + ", ");
			flownumber++;
			tickets.add(tickettemp);
		}
		System.out.println("谢谢您");
		//in.close();
		return;
	}
	
	static private void reserveGivenRow(){
		Scanner in = new Scanner(System.in);
		String sTemp;
		ArrayList<Ticket> ticketstemp;
		Ticket tickettemp;
		String playtime;
		String row;
		int nTemp;
		film f;
		u = new User();
		System.out.println("你好，你叫啥？");
		sTemp = in.nextLine();
		u.setName(sTemp);
		System.out.println("你的年龄？");
		nTemp = in.nextInt();
		u.setAge(nTemp);
		nuser++;
		u.setID(nuser);
		System.out.println("您要定哪一个电影？（ID）");
		sTemp = in.next();
		f = searchFilmID(sTemp);
		if(f==null){
			f = searchFilmName(sTemp);
		}
		if(f==null){
			System.out.println("哥们，没这个电影");
			//in.close();
			return;
		}
		if(u.getAge()<f.classification){
			System.out.println("年龄不够，无法订");
			//in.close();
			return;
		}
		System.out.println("您想要订几点场？（eg: 10:00）");
		playtime = in.next();
		int i;
		for(i=0;i<f.timelist.length;i++){
			if(playtime.equals(f.timelist[i]))
				break;
		}
		if(i == f.halls.length){
			System.out.println("哥们，没这场。");
			//in.close();
			return;
		}
		System.out.println("订几张？");
		nTemp = in.nextInt();
		System.out.println("订哪排(eg : D)");
		row = in.next();
		if(row.length()!=1){
			System.out.println("请输入单一大写字母。订票失败");
			return;
		}
		ticketstemp = f.halls[i].reserveGR(row.charAt(0),nTemp);
		if(ticketstemp==null){
			System.out.println("该排票数不够，订票失败");
			//in.close();
			return;
		}
		int k;
		System.out.print("出票成功，影票id：");;
		for(k=0;k<ticketstemp.size();k++){
			tickettemp = ticketstemp.get(k);
			tickettemp.set(flownumber,f.id, f.name, playtime,f.hall);
			System.out.print(flownumber + ", ");
			flownumber++;
			tickets.add(tickettemp);
		}
		System.out.println("谢谢您");
		//in.close();
		return;
	}
	
	static private void reserveGivenRegion(){
		Scanner in = new Scanner(System.in);
		String sTemp;
		String color;
		ArrayList<Ticket> ticketstemp;
		Ticket tickettemp;
		String playtime;
		int nTemp;
		film f;
		u = new User();
		System.out.println("你好，你叫啥？");
		sTemp = in.nextLine();
		u.setName(sTemp);
		System.out.println("你的年龄？");
		nTemp = in.nextInt();
		u.setAge(nTemp);
		nuser++;
		u.setID(nuser);
		System.out.println("您要定哪一个电影？（ID）");
		sTemp = in.next();
		f = searchFilmID(sTemp);
		if(f==null){
			f = searchFilmName(sTemp);
		}
		if(f==null){
			System.out.println("哥们，没这个电影");
			//in.close();
			return;
		}
		if(u.getAge()<f.classification){
			System.out.println("年龄不够，无法订");
			//in.close();
			return;
		}
		System.out.println("您想要订几点场？（eg: 10:00）");
		playtime = in.next();
		int i;
		for(i=0;i<f.timelist.length;i++){
			if(playtime.equals(f.timelist[i]))
				break;
		}
		if(i == f.halls.length){
			System.out.println("哥们，没这场。");
			//in.close();
			return;
		}
		System.out.println("订几张？");
		nTemp = in.nextInt();
		System.out.println("订哪个区域？（eg:red或blue或gray或yellow）");
		color = in.next();
		ticketstemp = f.halls[i].reserveGR(color,nTemp);
		if(ticketstemp==null){
			System.out.println("此区域票数不够，订票失败");
			//in.close();
			return;
		}
		int k;
		System.out.print("出票成功，影票id：");;
		for(k=0;k<ticketstemp.size();k++){
			tickettemp = ticketstemp.get(k);
			tickettemp.set(flownumber,f.id, f.name, playtime,f.hall);
			System.out.print(flownumber + ", ");
			flownumber++;
			tickets.add(tickettemp);
		}
		System.out.println("谢谢您");
		//in.close();
		return;
	}
	static private void reserveTicketWithConstrain(){
		Scanner in = new Scanner(System.in);
		String stemp;
		System.out.println("a.连续位置订票");
		System.out.println("b.指定排数订票");
		System.out.println("c.指定区域订票");
		System.out.println("请输入a或b或c");
		stemp = in.next();
		if(stemp.equals("a")){
			reserveSequentially();
			return;
		}
		else if(stemp.equals("b")){
			reserveGivenRow();
			return;
		}
		else if(stemp.equals("c")){
			reserveGivenRegion();
			return;
		}
		else{
			System.out.println("输入不认识。订票失败");
			return;
		}
		
	}

	static private void inquireByTime(){
		Scanner in = new Scanner(System.in);
		String bTime,eTime,sTemp;
		String [] temp;
		int bHour,bMinute,eHour,eMinute;
		int maxLength;
		int filmHour,filmMinute;
		System.out.println("最早播映时间（eg:10:00）");
		bTime = in.next();
		bTime.trim();
		temp = bTime.split(":");
		if(temp.length!=2){
			System.out.println("时间输入错误，查询失败");
			//in.close();
			return;
		}
		try{
			bHour = Integer.valueOf(temp[0]);
			bMinute = Integer.valueOf(temp[1]);
		}catch(Exception e){
			System.out.println("时间输入错误，查询失败");
			//in.close();
			return;
		}
		System.out.println("最晚播映时间（eg:13:00）");
		eTime = in.next();
		eTime.trim();
		temp = eTime.split(":");
		if(temp.length!=2){
			System.out.println("时间输入错误，查询失败");
			//in.close();
			return;
		}
		try{
			eHour = Integer.valueOf(temp[0]);
			eMinute = Integer.valueOf(temp[1]);
		}catch(Exception e){
			System.out.println("时间输入错误，查询失败");
			//in.close();
			return;
		}
		System.out.println("最长放映时长？（整数分钟）");
		maxLength = in.nextInt();
		int nfilm = films.size();
		int i,j;
		int flag = 0;
		for(i=0;i<nfilm;i++){
			flag = 0;
			if(films.get(i).length>maxLength)
				continue;
			for(j=0;j<films.get(i).timelist.length;j++){
				sTemp = films.get(i).timelist[j];
				temp = sTemp.split(":");
				if(temp.length!=2){
					System.out.println("对不起，系统错误，查询失败");
					//in.close();
					return;
				}
				try{
					filmHour = Integer.valueOf(temp[0]);
					filmMinute = Integer.valueOf(temp[1]);
				}catch(Exception e){
					System.out.println("对不起，系统错误，查询失败");
					//in.close();
					return;
				}
				if(bHour>filmHour||(bHour==filmHour&&bMinute>filmMinute)){
					continue;
				}
				else if(eHour<filmHour||(eHour==filmHour&&eMinute<filmMinute)){
					continue;
				}
				else{
					if(flag==0){
						flag = 1;
						System.out.println("");
						System.out.print("电影id "+films.get(i).id + "有满足场次：");
					}
					System.out.print(sTemp+",");
				}
			}
		}
		System.out.println("");
		System.out.println("查询结束。谢谢");
	}
	static private void inquireBySeats(){
		Scanner in = new Scanner(System.in);
		int n;
		System.out.println("需求座位数目？");
		n = in.nextInt();
		System.out.println("请问您要指定区域还是排数？（单个字母按排数匹配，否则匹配区域）");
		String sTemp = in.next();
		if(sTemp.length()==1){
			char row = sTemp.charAt(0);
			int nfilms = films.size();
			int i,j;
			int flag=0;
			for(i=0;i<nfilms;i++){
				flag = 0;
				for(j=0;j<films.get(i).halls.length;j++){
					if(films.get(i).halls[j].availableSeatsNumberAtGivenRow(row)>n){
						if(flag==0){
							flag = 1;
							System.out.println("");
							System.out.print("电影id "+films.get(i).id + "有满足场次：");
						}
						System.out.print(films.get(i).timelist[j]+", ");
					}
				}
			}
			System.out.println("");
			System.out.println("查询结束，谢谢");
		}
		else{
			char row = sTemp.charAt(0);
			int nfilms = films.size();
			int i,j;
			int flag=0;
			boolean succeed = false;
			for(i=0;i<nfilms;i++){
				flag = 0;
				for(j=0;j<films.get(i).halls.length;j++){
					if(sTemp.equals("gray"))
						succeed = (films.get(i).halls[j].getgray()>=n);
					else if(sTemp.equals("blue"))
						succeed = (films.get(i).halls[j].getblue()>=n);
					else if(sTemp.equals("yellow"))
						succeed = (films.get(i).halls[j].getyellow()>=n);
					else if(sTemp.equals("red"))
						succeed = (films.get(i).halls[j].getred()>=n);
					else{
						System.out.println("区域制定错误，无此区域。有：red,blue,gray,yellow");
						return;
					}
					if(succeed){
						if(flag==0){
							flag = 1;
							System.out.println("");
							System.out.print("电影id "+films.get(i).id + "有满足场次：");
						}
						System.out.print(films.get(i).timelist[j]+", ");
					}
				}
			}
			System.out.println("");
			System.out.println("查询结束，谢谢");
		}
	}
	static private void inquireByScore(){
		Scanner in = new Scanner(System.in);
		double sco;
		System.out.println("请输入分数：十分制小数");
		sco = in.nextDouble();
		int nfilm = films.size();
		int i;
		for(i=0;i<nfilm;i++){
			if(films.get(i).score1>sco){
				System.out.println("电影ID："+films.get(i).id);
			}
		}
		System.out.println("查询结束，谢谢");
	}
	static private void filmInformation(){
		Scanner in = new Scanner(System.in);
		String sTemp;
		film f;
		System.out.println("请输入电影的id");
		sTemp = in.next();
		f = searchFilmID(sTemp);
		if(f==null){
			f = searchFilmName(sTemp);
		}
		if(f==null){
			System.out.println("不好意思，没这个电影");
			//in.close();
			return;
		}
		System.out.println("电影ID："+f.id);
		System.out.println("电影名称："+f.name);
		if(f.classification==6){
			System.out.println("分级：保护");
		}
		else if(f.classification==15){
			System.out.println("分级：辅导");
		}
		else if(f.classification==18){
			System.out.println("分级：限制");
		}
		else{
			System.out.println("普通");
		}
		System.out.println("播放时间：");
		int j;
		for(j=0;j<f.timelist.length;j++){
			System.out.print(f.timelist[j] + ", ");
		}
		System.out.println("");
		System.out.println("厅位: "+f.hall);
		return;
	}
	static private void ticketInformation(){
		Scanner in = new Scanner(System.in);
		int id;
		System.out.println("请输入电影票ID");
		id = in.nextInt();
		int i;
		for(i=0;i<tickets.size();i++){
			if(tickets.get(i).ID==id){
				break;
			}
		}
		if(i==tickets.size()){
			System.out.println("没有这个电影票");
			return;
		}
		Ticket t = tickets.get(i);
		System.out.println("电影ID："+t.filmID);
		System.out.println("电影名称："+t.filmname);
		System.out.println("播映时间："+t.playtime);
		System.out.println("厅位："+t.hall);
		if(t.seatnumber<10){
			System.out.println("座位："+t.row+"_0"+t.seatnumber);
		}
		else{
			System.out.println("座位："+t.row+"_"+t.seatnumber);
		}
		return;
	}
	static private void inquire(){
		Scanner in = new Scanner(System.in);
		System.out.println("a.在限定时间内播放的电影列表");
		System.out.println("b.在限定座位有票的电影列表");
		System.out.println("c.评分几分以上的电影");
		System.out.println("d.电影资料");
		System.out.println("e.电影票资料");
		System.out.println("请输入单个字母：a或b或c或d或e");
		String sOption;
		sOption = in.next();
		char cOption;
		cOption = sOption.charAt(0);
		switch(cOption){
		case 'a':
			inquireByTime();
			break;
		case 'b':
			inquireBySeats();
			break;
		case 'c':
			inquireByScore();
			break;
		case 'd':
			filmInformation();
			break;
		case 'e':
			ticketInformation();
			break;
		}
	}
public static void main(String[] argvs){
	
	Scanner cin = new Scanner(System.in);
	int option = 2;
	flownumber = FlownumberDataBaseManage.FlownumberDataSearch();
	tickets = TicketDataBaseManage.GiveMeAllTicket();
	films = MovieDataBaseManage.GiveMeAllMovie();
	for(int i=0;i<tickets.size();i++){
		TicketDataBaseManage.TicketDelete(tickets.get(i).ID);
	}
	while(true){
		//cin = new Scanner(System.in);
		System.out.println("welcome");
		//System.out.println("1:add film information");
		System.out.println("1:reserve a ticket");
		System.out.println("2.return a ticket");
		System.out.println("3.reserve a ticket with constrains");
		System.out.println("4.inquire");
		System.out.println("5.leave");
		//if(cin.hasNext())
		try{	option = cin.nextInt();
		switch(option){
		case 1:
			reserveTicket();
			break;
		case 2:
			returnTicket();
			break;
		case 3:
			reserveTicketWithConstrain();
			break;
		case 4:
			inquire();
			break;
		case 5:
			System.out.println("再见");
			cin.close();
			for(int i=0;i<tickets.size();i++){
				TicketDataBaseManage.TicketInsert(tickets.get(i));
			}
			FlownumberDataBaseManage.FlownumberChange(flownumber);
			return;	
		default:
			System.out.println("请输入1-5");
			break;
		}
		}catch(Exception e){
			System.out.println("非法输入");
			cin.nextLine();
		}
	}
}

}
