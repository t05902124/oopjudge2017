import java.math.*;

public class Circle extends Shape{
	
	public Circle(double length){
		super(length);
	}
	public void setLength(double length){
		this.length = length;
	}
	public double getArea(){
		double area = this.length * this.length * Math.PI / 4;
		return (double)Math.round(area*100)/100;
	}
	public double getPerimeter(){
		double perimeter = Math.PI*this.length;
		return (double) Math.round(perimeter*100)/100;
	}
}