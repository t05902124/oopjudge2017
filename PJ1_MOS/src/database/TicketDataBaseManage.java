package database;
import java.sql.*;
import java.util.*;

import data_structure.*;
public class TicketDataBaseManage {

	
	
	public static void TicketInsert( Ticket t )
	  {
//	    Create();
		Connection c = null;
	    Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
	      c.setAutoCommit(false);

	      stmt = c.createStatement();
	      String sql = "INSERT INTO TICKET (ID,FILMNAME,FILMID,PLAYTIME,ROW,SEATNUMBER) " +
	                   "VALUES (" + t.ID + ", '" + t.filmname + "', '" + t.filmID + "', '" + t.playtime + "', '" + t.row + "', " + t.seatnumber + ");"; 
	      stmt.executeUpdate(sql);


	      stmt.close();
	      c.commit();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	  }
	public static void TicketDelete( int ID )
	  {{
		    Connection c = null;
		    Statement stmt = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
		      c.setAutoCommit(false);

		      stmt = c.createStatement();
		      String sql = "DELETE from TICKET where ID=" + ID + ";";
		      stmt.executeUpdate(sql);
		      c.commit();
		      
		      stmt.close();
		      c.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		  }	  }
	
	
	
	
	public static ArrayList<Ticket> TicketSearch(int ID){
		Connection c = null;
	    Statement stmt = null;
	    ArrayList<Ticket> r = new ArrayList<Ticket>();
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
	      c.setAutoCommit(false);

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM TICKET WHERE ID = " + ID +";" );
	      while ( rs.next() ) {
	         Ticket tmp = new Ticket(rs.getInt("ID"),rs.getString("FILMNAME"),rs.getString("FILMID"),rs.getString("PLAYTIME"),rs.getString("ROW").toCharArray()[0],rs.getInt("SEATNUMBER"));
	         r.add(tmp);
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    return r;
		}

	public static ArrayList<Ticket> GiveMeAllTicket(){
		Connection c = null;
	    Statement stmt = null;
	    ArrayList<Ticket> r = new ArrayList<Ticket>();
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:MovieSys.db");
	      c.setAutoCommit(false);

	      stmt = c.createStatement();
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM TICKET;" );
	      while ( rs.next() ) {
	         Ticket tmp = new Ticket(rs.getInt("ID"),rs.getString("FILMNAME"),rs.getString("FILMID"),rs.getString("PLAYTIME"),rs.getString("ROW").toCharArray()[0],rs.getInt("SEATNUMBER"));
	         r.add(tmp);
	      }
	      rs.close();
	      stmt.close();
	      c.close();
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    return r;
		}

}



