package data_structure;

import java.util.ArrayList;

public class smallhall implements hall{
	private int[][] seats;
	public smallhall(){
		seats = new int[9][17];
		int i,j;
		for(i=0;i<9;i++){
			seats[i][0] = -1;
			for(j=1;j<=16;j++){
				seats[i][j] = 0;
			}
		}
	}
	
	public boolean reserve(char row,int b){
		int a = row - 'A';
		if(row<'A'||row>'I'){
			//throw new reserveException(reserveException.Type.SEATILLEGAL);
			return false;
		}
		else if(b<=0||b>16){
			//throw new reserveException(reserveException.Type.SEATILLEGAL);
			return false;
		}
		else if(seats[a][b]==1){
			return false;
		}
		else{
			seats[a][b] = 1;
			return true;
		}
	}
	public boolean checkseats(char row, int b){
		int a = row - 'A';
		if(row<'A'||row>'I'){
			return false;
		}
		else if(b<=0||b>16){
			return false;
		}
		else if(seats[a][b]==1){
			return false;
		}
		else{
			return true;
		}
	}
	public int getblue(){
		return 0;
	}
	public int getgray(){
		int i,j;
		int n = 0;
		for(i=0;i<9;i++){
			for(j=0;j<17;j++){
				if(seats[i][j]==0)
					n++;
			}
		}
		return n;
	}
	public int getyellow(){
		return 0;
	}
	public int getred(){
		return 0;
	}
	public ArrayList<Ticket> reserven(int n){
		int j;
		char i;
		Ticket temp;
		ArrayList<Ticket> t = new ArrayList<Ticket>();
 		int count = 0;
		if(n<=0){
			System.out.println("小于等于零咋订票？");
			return null;
		}
		for(i='A';i<='I';i++){
			for(j = 0;j<17;j++){
				if(checkseats(i,j))
					count++;
			}
		}
		if(count<n){
			return null;
		}
		count = 0;
		for(i='A';i<='I';i++){
			for(j = 0;j<17;j++){
				if(count>=n){
					return t;
				}
				if(checkseats(i,j)){
					count++;
					reserve(i,j);
					temp = new Ticket(i,j);
					t.add(temp);
				}
					
			}
		}
		return t;
	}

	public boolean returnseat(char row,int b){
		int a = row - 'A';
		seats[a][b] = seats[a][b] - 1;
		return true;
	}
	
	public ArrayList<Ticket> reserveSqt(int n){
		int j;
		char i;
		Ticket temp;
		ArrayList<Ticket> t = new ArrayList<Ticket>();
 		int count = 0;
		if(n<=0){
			System.out.println("小于等于零咋订票？");
			return null;
		}
		for(i='A';i<='I';i++){
			count = 0;
			for(j = 0;j<40;j++){
				if(j==5||j==13)
					count = 0;
				if(checkseats(i,j))
					count++;
				else count = 0;
				if(count>=n){
					for(;count>0;count--){
						reserve(i,j);
						temp = new Ticket(i,j);
						t.add(temp);
						j--;
					}
					return t;
				}
			}
		}
		return null;
	}
	
	public ArrayList<Ticket> reserveGR(char row,int n){
		int j;
		Ticket temp;
		ArrayList<Ticket> t = new ArrayList<Ticket>();
 		int count = 0;
		if(n<=0){
			System.out.println("小于等于零咋订票？");
			return null;
		}
			for(j = 0;j<17;j++){
				if(checkseats(row,j))
					count++;
			}
		
		if(count<n){
			return null;
		}
		count = 0;
			for(j = 0;j<17;j++){
				if(count>=n){
					return t;
				}
				if(checkseats(row,j)){
					count++;
					reserve(row,j);
					temp = new Ticket(row,j);
					t.add(temp);
				}
					
			}
		return t;
	}
	
	public ArrayList<Ticket> reserveGR(String color,int n){
		if(!color.equals("gray")){
			System.out.println("这个电影在小厅，小厅只有gray座位");
			return null;
		}
		else return reserven(n);
	}
	public int availableSeatsNumberAtGivenRow(char row){
		int j;
		int count = 0;
		for(j=0;j<17;j++){
			if(checkseats(row,j)){
				count++;
			}
		}
		return count;
	}
}

