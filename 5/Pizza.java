public class Pizza {
	private String Size;
	private int NumberOfCheese;
	private int NumberOfPepperoni;
	private int NumberOfHam;
	public Pizza(){
		this.Size="small";
		this.NumberOfCheese = 1;
		this.NumberOfPepperoni = 1;
		this.NumberOfHam = 1;
	}
	public Pizza(String Size,int chztopping,int ppntopping,int hamtopping){
		if(!(Size.equals("small")||Size.equals("medium")||Size.equals("large"))){
			System.out.println("size set error");						//if the input has some error, inform the user
		}
		this.Size = Size;
		this.NumberOfCheese = chztopping;
		this.NumberOfPepperoni = ppntopping;
		this.NumberOfHam = hamtopping;
	}
	public void setSize(String Size){
		this.Size = Size;
	}
	public void setNumberOfCheese(int NumberOfCheese){
		this.NumberOfCheese = NumberOfCheese;
	}
	public void setNumberOfPepperoni(int NumberOfPepperoni){
		this.NumberOfPepperoni = NumberOfPepperoni;
	}
	public void setNumberOfHam(int NumberOfHam){
		this.NumberOfHam = NumberOfHam;
	}
	public String getSize(){
		return this.Size;
	}
	public int getNumberOfCheese(){
		return this.NumberOfCheese;
	}
	public int getNumberOfPepperoni(){
		return this.NumberOfPepperoni;
	}
	public int getNumberOfHam(){
		return this.NumberOfHam;
	}
	public double calcCost(){
		double cost = 0.0;
		if(this.Size.equals("small")){
			cost = 10;
		}
		else if(this.Size.equals("medium")){
			cost = 12;
		}
		else if(this.Size.equals("large")){
			cost = 14;
		}
		else{
			System.out.println("the size of this pizza error");
			return -1;
		}
		cost+=2*this.NumberOfCheese+2*this.NumberOfPepperoni+2*this.NumberOfHam;
		return cost;
	}
	public boolean equals(Pizza p){
		if(!this.Size.equals(p.Size)){
			return false;
		}
		if(this.NumberOfCheese!=p.NumberOfCheese){
			return false;
		}
		if(this.NumberOfPepperoni!=p.NumberOfPepperoni){
			return false;
		}
		if(this.NumberOfHam!=p.NumberOfHam){
			return false;
		}
		return true;
	}
	public String toString(){
		String result = "size = "+this.Size;
		Integer NumberOfCheese= this.NumberOfCheese;		//use the method in Interger toString to help us finish this job
		Integer NumberOfPepperoni = this.NumberOfPepperoni;
		Integer NumberOfHam = this.NumberOfHam;
		result +=", numOfCheese = " + NumberOfCheese.toString();
		result += ", numOfPepperoni = " + NumberOfPepperoni.toString();
		result +=", numOfHam = " + NumberOfHam.toString();
		return result;
	}

}