package mineSweeping;

import java.awt.Color;
import database.*;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

//游戏界面方格部分
public class Panel extends JPanel {
	private JButton[][] buttons;
	private int number; // 剩余雷数
	private int shownnumber; // 显示雷数
	private int[][] mine; // 存放各个格子的数据(若是雷则存放-1)
	private int hour, minute, second;
	private int time; // 用来存储从同一玩家当前最快时间
	private String userName;
	private Frame jf; // 外部传入的窗体，用于根据number来更改窗体上的数字

	public Panel(Frame jf) {
		try { // 把该程序的外观设置成当前所使用的平台的外观
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
		}

		setLayout(new GridLayout(16, 30));
		this.jf = jf;

		buttons = new JButton[16][30];
		for (int i = 0; i < 16; i++) {
			for (int j = 0; j < 30; j++) {
				buttons[i][j] = new JButton();
				buttons[i][j].setFont(new Font("宋体", Font.PLAIN, 50));
				add(buttons[i][j]);
			}
		}

		number = 99; // 共有99个雷
		shownnumber = 99;
		mine = new int[16][30];
		Random ran = new Random();
		int position = 0;
		for (int i = 0; i < 99; i++) {
			position = ran.nextInt(16 * 30); // 随机产生雷的位置
			if (mine[position / 30][position % 30] == -1) // 产生的99个雷的位置不允许重复
				i--; // 再i++后相当于i没变化
			else
				mine[position / 30][position % 30] = -1;
		}

		// 填入每格的数据
		for (int i = 0; i < 16; i++)
			for (int k = 0; k < 30; k++) {
				if (mine[i][k] != -1) // 不是雷
				{// 扫描周围8个格子，确定雷数
					for (int j = k - 1; j <= k + 1; j++) {
						if (j >= 0 && j < 30) // 在雷区内
						{
							if (mine[i][j] == -1)
								mine[i][k]++;

							if ((i - 1) >= 0) // 在雷区内
								if (mine[i - 1][j] == -1)
									mine[i][k]++;

							if ((i + 1) < 16) // 在雷区内
								if (mine[i + 1][j] == -1)
									mine[i][k]++;
						}
					}
				}
			}

		// 显示地雷位置（测试时使用）
//		for (int i = 0; i < 16; i++) {
//			for (int k = 0; k < 30; k++) {
//				System.out.printf("%3d", mine[i][k]);
//			}
//			System.out.printf("\n");
//		}

		buttonHandler handler = new buttonHandler();
		for (int i = 0; i < 16; i++)
			for (int j = 0; j < 30; j++)
				buttons[i][j].addMouseListener(handler);
	}

	// 匿名内部类,用于处理点击button后触发事件
	private class buttonHandler extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			for (int i = 0; i < 16; i++) // 确定是点击了哪个按钮
				for (int k = 0; k < 30; k++) {
					if (e.getSource() == buttons[i][k]) {
						if (e.getButton() == e.BUTTON1) // 左击
						{
							buttons[i][k].setForeground(Color.BLACK);
							if (buttons[i][k].getText().equals("#"))
								shownnumber++;
							JTextField jt = jf.getLeftNumber();
							jt.setText(" " + shownnumber);
							jf.setLeftNumber(jt);
							if ((!buttons[i][k].getText().equals("")) && (!buttons[i][k].getText().equals("#"))) {
								int minenum = 0;
								for (int j = k - 1; j <= k + 1; j++) {
									if (j >= 0 && j < 30) // 在雷区内
									{
										if (buttons[i][j].getText().equals("#"))
											minenum++;

										if ((i - 1) >= 0) // 在雷区内
											if (buttons[i - 1][j].getText().equals("#"))
												minenum++;

										if ((i + 1) < 16) // 在雷区内
											if (buttons[i + 1][j].getText().equals("#"))
												minenum++;
									}
								}
								if (minenum == mine[i][k]) {
									int flag = 0;
									for (int j = k - 1; j <= k + 1; j++) {
										if (j >= 0 && j < 30) // 在雷区内
										{
											if (buttons[i][j].getText().equals("#") && mine[i][j] != -1)
												flag++;

											if ((i - 1) >= 0) // 在雷区内
												if (buttons[i - 1][j].getText().equals("#") && mine[i - 1][j] != -1)
													flag++;

											if ((i + 1) < 16) // 在雷区内
												if (buttons[i + 1][j].getText().equals("#") && mine[i + 1][j] != -1)
													flag++;
										}
									}
									if (flag > 0) {
										for (int j = 0; j < 16; j++) // 所有方格打开
										{
											for (int l = 0; l < 30; l++) {
												if (mine[j][l] == -1) {
													if (buttons[j][l].getText().equals("#")) // 此处被玩家标记为雷(玩家标记正确)
													{
														buttons[j][l].setForeground(Color.BLACK);
														buttons[j][l].setText("Y"); // 表示该处扫雷成功
													} else
														buttons[j][l].setText("*"); // *表示雷
												}
												// else
												// buttons[j][l].setText(Integer.toString(mine[j][l]));
												buttons[j][l].setEnabled(false); // 游戏结束，所有按钮都不可点击
											}
										}
										JOptionPane.showMessageDialog(null, "踩到雷啦！", "游戏结束", JOptionPane.ERROR_MESSAGE);
									} else {
										for (int j = k - 1; j <= k + 1; j++) {
											if (j >= 0 && j < 30) // 在雷区内
											{
												if (buttons[i][j].getText().equals(""))
													buttons[i][j].setText("" + mine[i][j]);

												if ((i - 1) >= 0) // 在雷区内
													if (buttons[i - 1][j].getText().equals(""))
														buttons[i - 1][j].setText("" + mine[i - 1][j]);

												if ((i + 1) < 16) // 在雷区内
													if (buttons[i + 1][j].getText().equals(""))
														buttons[i + 1][j].setText("" + mine[i + 1][j]);
											}
										}
									}

								}

							} else if (mine[i][k] == -1) // 踩到雷
							{
								for (int j = 0; j < 16; j++) // 所有方格打开
								{
									for (int l = 0; l < 30; l++) {
										if (mine[j][l] == -1) {
											if (buttons[j][l].getText().equals("#")) // 此处被玩家标记为雷(玩家标记正确)
											{
												buttons[j][l].setForeground(Color.BLACK);
												buttons[j][l].setText("Y"); // 表示该处扫雷成功
											} else
												buttons[j][l].setText("*"); // *表示雷
										}
										// else
										// buttons[j][l].setText(Integer.toString(mine[j][l]));
										buttons[j][l].setEnabled(false); // 游戏结束，所有按钮都不可点击
									}
								}
								JOptionPane.showMessageDialog(null, "踩到雷啦！", "游戏结束", JOptionPane.ERROR_MESSAGE);
							} else { // 没有踩到雷，则显示该空格周围8个空格中雷的数目
								buttons[i][k].setText("" + mine[i][k]);
								if (mine[i][k] == 0) {
									Queue<Integer> x1 = new LinkedList<Integer>();
									Queue<Integer> y1 = new LinkedList<Integer>();
									x1.offer(i);
									y1.offer(k);
									mine[i][k] = -2;
									while (x1.size() > 0) {
										for (int j = y1.element() - 1; j <= y1.element() + 1; j++) {
											if (j >= 0 && j < 30) // 在雷区内
											{
												if (mine[x1.element()][j] == 0) {
													x1.offer(x1.element());
													y1.offer(j);
													mine[x1.element()][j] = -2;
												}
												if (buttons[i][k].getText().equals("#")) {
													shownnumber++;
													buttons[i][k].setForeground(Color.BLACK);
												}
												jt = jf.getLeftNumber();
												jt.setText(" " + shownnumber);
												jf.setLeftNumber(jt);
												if (mine[x1.element()][j] == -2)
													buttons[x1.element()][j].setText("" + 0);
												else
													buttons[x1.element()][j].setText("" + mine[x1.element()][j]);

												if ((x1.element() - 1) >= 0) // 在雷区内
												{
													if (mine[x1.element() - 1][j] == 0) {
														x1.offer(x1.element() - 1);
														y1.offer(j);
														mine[x1.element() - 1][j] = -2;
													}
													if (buttons[i][k].getText().equals("#")) {
														shownnumber++;
														buttons[i][k].setForeground(Color.BLACK);
													}
													jt = jf.getLeftNumber();
													jt.setText(" " + shownnumber);
													jf.setLeftNumber(jt);
													if (mine[x1.element() - 1][j] == -2)
														buttons[x1.element() - 1][j].setText("" + 0);
													else
														buttons[x1.element() - 1][j]
																.setText("" + mine[x1.element() - 1][j]);
												}

												if ((x1.element() + 1) < 16) // 在雷区内
												{
													if (mine[x1.element() + 1][j] == 0) {
														x1.offer(x1.element() + 1);
														y1.offer(j);
														mine[x1.element() + 1][j] = -2;
													}
													if (buttons[i][k].getText().equals("#")) {
														shownnumber++;
														buttons[i][k].setForeground(Color.BLACK);
													}
													jt = jf.getLeftNumber();
													jt.setText(" " + shownnumber);
													jf.setLeftNumber(jt);
													if (mine[x1.element() + 1][j] == -2)
														buttons[x1.element() + 1][j].setText("" + 0);
													else
														buttons[x1.element() + 1][j]
																.setText("" + mine[x1.element() + 1][j]);
												}
											}
										}

										x1.poll();
										y1.poll();
									}
								}
							}

						} else if (e.getButton() == e.BUTTON3) { // 右击(玩家认为此处是雷的位置)
							if (buttons[i][k].getText().equals("#")) {
								shownnumber++;
								if (mine[i][k] == -1)
									number++;
								JTextField jt = jf.getLeftNumber();
								jt.setText(" " + shownnumber);
								jf.setLeftNumber(jt);
								buttons[i][k].setText("");
							} else if (buttons[i][k].getText().equals("")) {

								buttons[i][k].setForeground(Color.RED);
								buttons[i][k].setText("#");
								shownnumber--;
								if (mine[i][k] == -1) // 若此处确实是雷
								{
									number--; // 剩余雷数-1
								}
								// 更改窗体上的剩余雷数
								JTextField jt = jf.getLeftNumber();
								jt.setText(" " + shownnumber);
								jf.setLeftNumber(jt);

								if (number == 0) // 已经扫除了全部的雷(胜利-游戏结束)
								{
									for (int j = 0; j < 16; j++) // 所有方格打开
									{
										for (int l = 0; l < 30; l++) {
											if (mine[j][l] == -1) {
												if (buttons[j][l].getText().equals("#")) // 此处被玩家标记为雷(玩家标记正确)
												{
													buttons[j][l].setForeground(Color.BLACK);
													buttons[j][l].setText("/*/"); // 表示该处扫雷成功
												} else
													buttons[j][l].setText("*"); // *表示雷
											}
											buttons[j][l].setEnabled(false); // 游戏结束，所有按钮都不可点击
										}
									}
									JOptionPane.showMessageDialog(null, "你赢啦！", "游戏结束", JOptionPane.PLAIN_MESSAGE);

									// 记录游戏者的最快完成时间到文件中:按照“用户名 最快完成时间”的形式写入文件
									Calendar ca = Calendar.getInstance(); // 获取当前系统时间
									int h = ca.get(Calendar.HOUR) - hour;
									int m = ca.get(Calendar.MINUTE) - minute;
									int s = ca.get(Calendar.SECOND) - second;
									int totalTime = h * 3600 + m * 60 + s; // 花费的总时间
//									if (time == 0) // 第一次玩
//									{
//										time = totalTime;
//										// 写入文件
//										InsertToFile insert = new InsertToFile();
//										insert.openFile();
//										insert.addRecords(userName, time);
//										insert.closeFile();
//									} else if (totalTime < time) // 更新最快时间
//									{
//										time = totalTime;
//										// 写入文件
//										InsertToFile insert = new InsertToFile();
//										insert.openFile();
//										insert.addRecords(userName, time);
//										insert.closeFile();
//									}
									Record r = new Record();
									r.setName(userName);
									r.setTime(totalTime);
									database.DataBaseManage.Insert(r);
								}
							}else if (buttons[i][k].getText().equals("!")){
								buttons[i][k].setForeground(Color.RED);
								buttons[i][k].setText("#");
								shownnumber--;
								number--; // 剩余雷数-1
								}
								// 更改窗体上的剩余雷数
								JTextField jt = jf.getLeftNumber();
								jt.setText(" " + shownnumber);
								jf.setLeftNumber(jt);

								if (number == 0) // 已经扫除了全部的雷(胜利-游戏结束)
								{
									for (int j = 0; j < 16; j++) // 所有方格打开
									{
										for (int l = 0; l < 30; l++) {
											if (mine[j][l] == -1) {
												if (buttons[j][l].getText().equals("#")) // 此处被玩家标记为雷(玩家标记正确)
												{
													buttons[j][l].setForeground(Color.BLACK);
													buttons[j][l].setText("/*/"); // 表示该处扫雷成功
												} else
													buttons[j][l].setText("*"); // *表示雷
											}
											buttons[j][l].setEnabled(false); // 游戏结束，所有按钮都不可点击
										}
									}
									JOptionPane.showMessageDialog(null, "你赢啦！", "游戏结束", JOptionPane.PLAIN_MESSAGE);

									// 记录游戏者的最快完成时间到文件中:按照“用户名 最快完成时间”的形式写入文件
									Calendar ca = Calendar.getInstance(); // 获取当前系统时间
									int h = ca.get(Calendar.HOUR) - hour;
									int m = ca.get(Calendar.MINUTE) - minute;
									int s = ca.get(Calendar.SECOND) - second;
									int totalTime = h * 3600 + m * 60 + s; // 花费的总时间

							}
						}
						break;
					}
				}
		}
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setShownNumber(int number) {
		this.shownnumber = number;
	}

	public int getShownNumber() {
		return this.shownnumber;
	}

	public int getNumber() {
		return number;
	}

	public JButton[][] getButtons() {
		return buttons;
	}

	public void setButtons(JButton[][] buttons) {
		this.buttons = buttons;
	}

	public int[][] getMine() {
		return mine;
	}

	public void setMine(int[][] mine) {
		this.mine = mine;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void cheating() {
		for (int i = 0; i < 16; i++)
			for (int j = 0; j < 30; j++)
				if (mine[i][j] == -1 && !buttons[i][j].getText().equals("#")){
					buttons[i][j].setText("!");
				}

	}
}