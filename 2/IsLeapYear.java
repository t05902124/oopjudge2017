
public class IsLeapYear {
	public boolean determine(int x){
		if(x%400==0)
			return true;
		else if(x%100==0)
			return false;
		else if(x%4==0)
			return true;
		else return false;
	}
}
