package database;

public class Record {
	int time;
	String name;
	
	public void setTime(int t){
		this.time = t;
	}
	public void setName(String s){
		this.name = s;
	}
	public int getTime(){
		return time;
	}
	public String getName(){
		return name;
	}
}
