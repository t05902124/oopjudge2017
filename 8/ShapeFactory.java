public class ShapeFactory {
	public enum Type{
		Triangle,Circle,Square;
	}
	
	public Shape createShape(ShapeFactory.Type shapeType,double length){
		
		if(shapeType==Type.Circle){
			Circle o= new Circle(length);
			return o;
		}
		else if(shapeType == Type.Triangle){
			Triangle o = new Triangle(length);
			return o;
		}
		else if(shapeType ==Type.Square){
			Square o = new Square(length);
			return o;
		}
		else return null;
	}
}