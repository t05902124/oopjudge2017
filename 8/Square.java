import java.math.*;

public class Square extends Shape{
	
	public Square(double length){
		super(length);
	}
	public void setLength(double length){
		this.length = length;
	}
	public double getArea(){
		double area = this.length*this.length;
		return (double)Math.round(area*100)/100;
	}
	public double getPerimeter(){
		double perimeter = 4*this.length;
		return (double) Math.round(perimeter*100)/100;
	}

}
