package data_structure;
import java.util.*;

public interface hall {
	public boolean reserve(char row,int b) throws reserveException;
	public boolean checkseats(char row, int b);
	public int getblue();
	public int getgray();
	public int getyellow();
	public int getred();
	public ArrayList<Ticket> reserven(int n);
	public boolean returnseat(char row,int b);
	public ArrayList<Ticket> reserveSqt(int n);//sequential
	public ArrayList<Ticket> reserveGR(char row,int n);//given row
	public ArrayList<Ticket> reserveGR(String color,int n);
	public int availableSeatsNumberAtGivenRow(char row);
}
