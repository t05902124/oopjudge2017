import java.text.DecimalFormat;

public class SimpleCalculator {
	private String operator;
	private double number;
	private double result;
	private int count;
	
	public SimpleCalculator(){
		this.result = 0.0;
		this.count = 0;
	}
	
	private void parse(String cmd) throws UnknownCmdException{
		String [] cmd_parse = cmd.split(" ");
		if(cmd_parse.length!=2){
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		}
		if(cmd_parse[0].equals("+")||cmd_parse[0].equals("-")||cmd_parse[0].equals("*")||cmd_parse[0].equals("/")){
			this.operator = cmd_parse[0];
			String temp = cmd_parse[1];
			int dot = 0;
			for(int i=0;i<temp.length();i++){
				if(Character.isDigit(temp.charAt(i))){
					continue;
				}
				else{
					if(temp.charAt(i)=='.'){
						dot++;
					}
					else{
						throw new UnknownCmdException(cmd_parse[1]+" is an unknown value");
					}
					if(dot>1){
						throw new UnknownCmdException(cmd_parse[1]+" is an unknown value");
					}
				}
			}
			this.number = Double.parseDouble(cmd_parse[1]);
		}
		else{
			String temp = cmd_parse[1];
			int dot = 0;
			for(int i=0;i<temp.length();i++){
				if(Character.isDigit(temp.charAt(i))){
					continue;
				}
				else{
					if(temp.charAt(i)=='.'){
						dot++;
					}
					else{
						throw new UnknownCmdException(cmd_parse[0]+" is an unknown operator and "+cmd_parse[1]+" is an unknown value");
					}
					if(dot>1){
						throw new UnknownCmdException(cmd_parse[0]+" is an unknown operator and "+cmd_parse[1]+" is an unknown value");
					}
				}
			}
			throw new UnknownCmdException(cmd_parse[0] + " is an unknown operator");
		}
	}
	
	private void calculate() throws UnknownCmdException{
		if(operator.equals("+")){
			result = result + number;
		}
		else if(operator.equals("-")){
			result = result - number;
		}
		else if(operator.equals("*")){
			result = result * number;
		}
		else{
			if(Math.abs(number - 0)<0.00000000001){
				throw new UnknownCmdException("Can not divide by 0");
			}
			else{
				result = result / number;
			}
		}
	}
	
	public void calResult(String cmd) throws UnknownCmdException{
		parse(cmd);
		calculate();
		count++;
	}
	
	public boolean endCalc(String cmd){
		if(cmd.equals("r")||cmd.equals("R")){
			count = -1;
			return true;
		}
		else{
			return false;
		}
	}

	public String getMsg(){
		if(count==0){
			return ("Calculator is on. Result = " + new DecimalFormat("0.00").format(result));
		}
		else if(count==1){
			return ("Result "+operator+" "+new DecimalFormat("0.00").format(number)+" = "+new DecimalFormat("0.00").format(result)+". New result = "+new DecimalFormat("0.00").format(result));
		}
		else if(count==-1){
			return ("Final result = " + new DecimalFormat("0.00").format(result));
		}
		else{
			return ("Result "+operator+" "+new DecimalFormat("0.00").format(number)+" = "+new DecimalFormat("0.00").format(result)+". Updated result = "+new DecimalFormat("0.00").format(result));
		}
	}
}
